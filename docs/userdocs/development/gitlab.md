To learn how to use containers and :fontawesome-brands-docker:Docker on your local machine, refer to our [tutorial section](/userdocs/tutorial/docker/).

We use our own installation of [:fontawesome-brands-gitlab:GitLab][1] for [Source Code Management][2], [Continuous Integration automation][3], 
containers registry and other development lifecycle tasks. It fully uses Nautilus Cluster resources, which provides our users unlimited storage and fast builds.
All data from our GitLab except container images are backed up nightly to Google storage, which means there's almost zero chance that you might lose your code in our repository. 

#### Step 1: Create a Git repo
1. To use our GItLab installation, register at [https://gitlab.nrp-nautilus.io][4]
1. Use GitLab for storing your code like any git repository. Here's [GitLab basics guide][5].
1. [Create a new project][project] in your GitLab account 

#### Step 2: Use Containers Registry
What makes GitLab especially useful for kubernetes cluster in integration with
Containers Registry. You can store your containers directly in our cluster and
avoid slow downloads from [DockerHub][dockerhub] (although you're still free to do that as well).

If you wish to use our registry, in your <https://gitlab.nrp-nautilus.io> project go to `Packages & Registries -> Container Registry` menu and read instructions on how to use one.

#### Step 3: Continuous Integration automation
To fully unleash the GitLab powers, introduce yourself to [Continuous Integration automation][3] 

1. Create the `.gitlab-ci.yml` file in your project, see [Quick start guide][quickstart]. The runners are already configured.  
   There's a list of CI templates available for most common languages.
1. If you need to build your Dockerfile and create a container from it, adjust this `.gitlab-ci.yml` template:

        image: gcr.io/kaniko-project/executor:debug
        
        stages:
          - build-and-push
        
        build-and-push-job:
          stage: build-and-push
          script:
            - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
            - /kaniko/executor --cache=true --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/Dockerfile --destination $CI_REGISTRY_IMAGE:${CI_COMMIT_SHA:0:8} --destination $CI_REGISTRY_IMAGE:latest

   [More advanced example][portal_example]

1. Go to `CI / CD -> Jobs` tab to see in amazement your job running and image being uploaded to your registry. 
1. From the `Packages -> Containers Registry` tab get the URL of your image to be included in your pod definition:

        spec:
          containers:
          - name: my-container
            image: gitlab-registry.nrp-nautilus.io/<your_group>/<your_project>:<optional_tag>

## Build better containers

Make yourself familiar with [Docker containers best practices](https://www.docker.com/blog/intro-guide-to-dockerfile-best-practices)

Use [multi-stage builds](https://docs.docker.com/develop/develop-images/multistage-build) when necessary

## Use S3 to store large files collections and access those during builds

Refer to [S3 documentation](/userdocs/storage/ceph-s3/#using-s3-in-gitlab-ci)

## Other development information

Check out [this guide](https://guide.esciencecenter.nl) from the Netherlands eScience Center for best practices in developing academic code.

[1]: https://about.gitlab.com/what-is-gitlab/
[2]: https://about.gitlab.com/product/source-code-management/ 
[3]: https://about.gitlab.com/product/continuous-integration/ 
[4]: https://gitlab.nrp-nautilus.io
[5]: https://docs.gitlab.com/ee/gitlab-basics/
[dockerhub]: https://docs.docker.com/docker-hub/
[project]: https://docs.gitlab.com/ee/user/project/working_with_projects.html#create-a-project
[registry]: https://docs.gitlab.com/ee/user/project/container_registry.html
[quickstart]: https://docs.gitlab.com/ce/ci/quick_start/
[portal_example]: https://gitlab.nrp-nautilus.io/prp/k8s_portal/blob/master/.gitlab-ci.yml
