To attach the  CVMFS volume which can mount all repos, create the PVC (taken from [https://github.com/cvmfs-contrib/cvmfs-csi/tree/master/example](https://github.com/cvmfs-contrib/cvmfs-csi/tree/master/example) ):

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: cvmfs
spec:
  accessModes:
  - ReadOnlyMany
  resources:
    requests:
      # Volume size value has no effect and is ignored
      # by the driver, but must be non-zero.
      storage: 1
  storageClassName: cvmfs
```

Then attach it to your pod:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: cvmfs-all-repos
spec:
  containers:
   - name: idle
     image: busybox
     imagePullPolicy: IfNotPresent
     command: [ "/bin/sh", "-c", "trap : TERM INT; (while true; do sleep 1000; done) & wait" ]
     volumeMounts:
       - name: my-cvmfs
         mountPath: /my-cvmfs
         # CVMFS automount volumes must be mounted with HostToContainer mount propagation.
         mountPropagation: HostToContainer
  volumes:
   - name: my-cvmfs
     persistentVolumeClaim:
       claimName: cvmfs
```

If you need to mount a specific repo, add the subPath to the pod mount:

```yaml
     volumeMounts:
       - name: my-cvmfs
         # It is possible to mount a single CVMFS repository by specifying subPath.
         subPath: alice.cern.ch
         mountPath: /my-alice-cvmfs
         mountPropagation: HostToContainer
```